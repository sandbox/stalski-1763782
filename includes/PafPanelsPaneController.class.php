<?php
/**
 * @file
 *
 * Contains the controller class for the Fieldable Panel Pane entity.
 */

/**
 * Entity controller class.
 */
class PafPanelsPaneController extends PanelsPaneController {
  public $entity;

  /**
   * Load the entity
   * @see DrupalDefaultEntityController::load()
   */
  public function load($ids = array(), $conditions = array()) {

    // Load from session, if it exists.
    if (count($ids) == 1 && isset($_SESSION['paf']['fpp_panes'][$ids[0]])) {

      $fpp_entity = $_SESSION['paf']['fpp_panes'][$ids[0]];
      $entities = array($ids[0] => $fpp_entity);

      // Load options.
      $null = NULL;
      $options = array(
        'deleted' => FALSE,
      );
      $age = FIELD_LOAD_CURRENT;

      // Invoke hook_field_load().
      _field_invoke_multiple('load', 'fieldable_panels_pane', $entities, $age, $null, $options);

      // Invoke hook_field_attach_load(): let other modules act on loading the
      // entitiy.
      module_invoke_all('field_attach_load', 'fieldable_panels_pane', $entities, FIELD_LOAD_CURRENT, $options);

      return $entities;
    }

    return parent::load($ids, $conditions);

  }

  /**
   * Save the entity.
   * @see PanelsPaneController::save()
   */
  public function save($entity) {

    $entity = (object) $entity;
     // Determine if we will be inserting a new entity.
    $entity->is_new = !(isset($entity->fpid) && is_numeric($entity->fpid));

    // Save all edited entities in session, don't save it to database.
    // This is needed because of revisions. paf_field_insert and paf_field_update will save it.
    if (arg(2) == 'paf_editor') {
      if ($entity->is_new) {
        $entity->fpid = 0 - $_SERVER['REQUEST_TIME'];
      }
      $_SESSION['paf']['fpp_panes'][$entity->fpid] = $entity;
      return $entity;
    }

    return parent::save($entity);

  }

}
