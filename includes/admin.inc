<?php

/**
 * @file
 * Administrative functions for PAF.
 */

/**
 * Menu callback: present the panels configuration settings
 * for the general PAF settings.
 */
function paf_panels_add_content_settings() {
  ctools_include('common', 'panels');
  $base_form = drupal_get_form('panels_common_settings', 'paf_general');
  return $base_form;
}

/**
 * Menu callback: present the panels configuration settings
 * for the PAF content selector.
 */
function paf_panels_content_selector_settings() {
  ctools_include('common', 'panels');
  $base_form = drupal_get_form('panels_common_settings', 'paf_selector');
  return $base_form;
}