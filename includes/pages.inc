<?php

/**
 * @file
 * Page callbacks for paf.
 */

/**
 * Page callback to show the display editor for current field.
 */
function paf_page_field_display_editor($entity_type, $bundle, $field_name, $did) {

  ctools_include('common', 'panels');
  ctools_include('plugins', 'panels');
  ctools_include('display-edit', 'panels');
  ctools_include('ajax');

  $field_instance = field_info_instance($entity_type, $field_name, $bundle);
  $settings = $field_instance['settings'];

  $cache_key = 'panelasfield-' . $did;
  $cached_display = panels_edit_cache_get($cache_key);
  if (!$cached_display) {
    // Retrieve the panel display.
    if (!empty($did) && is_numeric($did)) {
      $display = panels_load_display($did);
    }
    // Create the panel display.
    else {
      $display = panels_new_display();
      $display->renderer = 'paf';
      $display->did = $did;
    }
  }
  else {
    $display = $cached_display->display;
  }

  $display->fieldable_panels_pane_conf = $settings['container_other']['fieldable_panels_pane_conf'];
  $display->region_links_style = $settings['container_panels']['region_links'];
  $display->has_display_settings = $settings['container_panels']['hide_display_settings'];
  $display->pane_links_style = $settings['container_panels']['pane_links'];

  // Cache key.
  $display->cache_key = $cache_key;

  // Layout.
  $display->layout = $display->region_links_style == PAF_REGION_LINKS_CONTENT_SELECTOR ? $settings['container_panels']['layout_selector'] : $settings['container_panels']['layout_general'];

  // Context.
  $display->context = _paf_get_entity_context($entity_type);

  // Set the cache object.
  $cache = _paf_create_cache_object($display, $cache_key, $settings['container_panels']['region_links']);

  $form = drupal_get_form('paf_display_editor_form', $display, $field_name, $settings, $cache);
  $form['#attached']['js'][] = drupal_get_path('module', 'paf') . '/js/paf_editor.js';

  return $form;
}

/**
 * Form callback: Show the display editor for paf.
 */
function paf_display_editor_form($form, &$form_state, $display, $field_name, $settings, $cache) {

  // Include pages.inc for submit/cancel buttons.
  form_load_include($form_state, 'inc', 'paf', 'includes/pages');

  $form_state['renderer'] = panels_get_renderer_handler('paf_editor', $display);
  $form_state['renderer']->cache = &$cache;
  $form_state['display'] = $display;
  $form_state['content_types'] = $cache->content_types;
  $form_state['field_name'] = $field_name;

  // Tell the Panels form not to display buttons and title settings.
  $form_state['display_title'] = FALSE;
  $form_state['no preview'] = TRUE;

  $form = array();
  $form = panels_edit_display_form($form, $form_state);

  // Hide display settings if configured.
  if ($settings['container_panels']['hide_display_settings']) {
    $form['hide']['display-settings']['#access'] = FALSE;
  }

  // Change the buttons position and add wrapper.
  $form['buttons']['#weight'] = 0.000;
  $form['buttons']['#prefix'] = '<div class="paf-editor-buttons">';
  $form['buttons']['#suffix'] = '</div>';

  // Ajaxify the buttons.
  $form['buttons']['submit']['#ajax'] = array(
    'callback' => 'paf_display_editor_submit',
    'event' => 'mousedown',
    'keypress' => TRUE,
  );
  $form['buttons']['cancel']['#ajax'] = array(
    'callback' => 'paf_display_editor_cancel',
    'event' => 'mousedown',
    'keypress' => TRUE,
  );

  // No submit handlers to prevent fatal errors from missing includes.
  unset($form['buttons']['submit']['#submit']);

  // Add custom css.
  $form['#attached']['css'][] = drupal_get_path('module', 'paf') . '/css/paf_editor.css';

  // Make sure draggable library is there.
  $form['#attached']['library'][] = array('system', 'ui.draggable');

  // Add selector box.
  if ($settings['container_panels']['region_links'] == PAF_REGION_LINKS_CONTENT_SELECTOR) {
    $form[] = array('#markup' => _paf_content_selector($display, panels_common_get_allowed_types('paf_selector', $display->context), $settings));
  }

  $form['buttons_bottom'] = $form['buttons'];
  $form['buttons_bottom']['submit']['#id'] = 'panels-dnd-savebottom';
  $form['buttons_bottom']['cancel']['#id'] = 'panels-dnd-cancelbottom';
  $form['buttons_bottom']['#weight'] = 0.009;
  $form['buttons_bottom']['#prefix'] = '<div class="paf-editor-buttons-bottom">';

  return $form;
}

/**
 * Ajax submit callback: Save the display settings.
 */
function paf_display_editor_submit($form, &$form_state) {

  ctools_include('common', 'panels');
  ctools_include('plugins', 'panels');
  ctools_include('display-edit', 'panels');

  // Get the cached display. Panels changes cache when content has been added.
  $cache = panels_edit_cache_get($form_state['display']->cache_key);
  $form_state['display'] = $cache->display;

  // Load the renderer, before submit occures.
  $renderer = panels_get_renderer($cache->display->renderer, $cache->display);

  // Construct new display.
  panels_edit_display_form_submit($form, $form_state);

  // Set the cache.
  $cache->display = $form_state['display'];
  panels_edit_cache_set($cache);

  // Render the preview
  $content = _paf_render_display($cache->display);

  // Execute our javascript commands.
  $output = array();
  $output[] = ajax_command_invoke(NULL, 'pafRefreshPreview', array($form_state['field_name'], $content));
  $output[] = ajax_command_invoke(NULL, 'pafDialogClose');

  print ajax_render($output);
  exit;
}

/**
 * Ajax submit callback. Cancel the editing.
 */
function paf_display_editor_cancel($form, &$form_state) {

  // Delete the cache, all changes will be lost.
  panels_cache_clear('display', $form_state['display']->cache_key);
  unset($_SESSION['paf']);

  // Execute close command.
  $output = array();
  $output[] = ajax_command_invoke(NULL, 'pafDialogClose');

  print ajax_render($output);
  exit;
}

/**
 * Get entity context.
 */
function _paf_get_entity_context($entity_type) {
  ctools_include('context');
  $arguments = array(
    array(
      'keyword' => $entity_type,
      'identifier' => drupal_ucfirst($entity_type) . ' being viewed',
      'id' => 1,
      'name' => 'entity_id:' . $entity_type,
      'settings' => array(),
    ),
  );

  return ctools_context_get_placeholders_from_argument($arguments);
}

/**
 * Get the content types configured for the content selector.
 */
function _paf_content_selector($display, $content_types, $settings = array()) {
  $content = '';

  // Draggable interface is enabled when at least two regions are used in the
  // layout or if configured that way.
  $regions = array_keys($display->panels);
  $draggables_enabled = !empty($settings['container_panels']['draggable']) || count($regions) > 1;

  foreach ($content_types as $type_name => $subtypes) {
    foreach ($subtypes as $subtype_name => $content_type) {

      $title = check_plain($content_type['title']);

      $button = '<div class="content-item panel-pane">';

      if ($draggables_enabled) {
        $button .= '<div class="grabber"><span class="text"></span></div>';
      }

      $variables = array(
        'path' => ctools_content_admin_icon($content_type),
        'title' => $title,
        'alt' => $title,
      );
      $image_html = theme('image', $variables);
      // @todo middle is hardcoded first region now. JS should trigger the dropzone as
      // url replacement in this uri.
      $region_name = $draggables_enabled ? "-region-" : key($display->panels);
      $uri = 'panels/ajax/paf_editor/add-pane/' . $display->cache_key . '/' . $region_name . '/' . $type_name . '/' . $subtype_name;
      $attributes =  array('attributes' => array('alt' => $title, 'class' => array('paf-use-modal')));

      if ($draggables_enabled) {
        $attributes['attributes']['class'][] = 'draggable';
      }
      $button .= l($image_html, $uri, $attributes + array('html' => TRUE));

      if (empty($settings['container_panels']['icons_only'])) {
        $button .= l($title, $uri, $attributes);
      }

      $button .= '</div>';

      // Add to content.
      $content .= $button;
    }
  }

  if (!empty($content)) {
    return '<div id="panel-region-pafselector" class="paf-content-selector panel-region clearfix">' . $content . '</div>';
  }
}
