(function ($) {

  Drupal.Paf = {};
  Drupal.Paf.dialogContainer = null;
  
  /**
   * Implement Drupal behavior for autoattach
   */
  Drupal.behaviors.pafFieldDialog = {
    attach: function(context) {
      // Add dialog listener on paf links.
      $(context).find('a.paf-dialog').bind('click', Drupal.Paf.openDialog);
    }
  }
  
  /**
   * Click listener: Open the dialog for clicked paf link.
   */
  Drupal.Paf.openDialog = function(e) {
    
    e.preventDefault();
    var page = $(this).attr("href");
    var pagetitle = $(this).attr("title");
    Drupal.Paf.dialogContainer = $('<div id="paf-modal-container"></div>')
      .html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>')
      .dialog({
        autoOpen: true,
        closeOnEscape : false,
        dialogClass : 'paf-dialog',
        modal: true,
        height: (($(window).height() / 100) * 92),
        width: (($(window).width() / 100) * 90),
        title: pagetitle
      });
    
  }
  
})(jQuery);