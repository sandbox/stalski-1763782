(function ($) {

  Drupal.Paf = Drupal.Paf || {};
  
  /**
   * jQuery.pafDialogClose().
   * Closes the PAF dialog.
   */
  $.fn.pafDialogClose = function () {
    parent.Drupal.Paf.dialogContainer.dialog('close');
  };
   
  /**
   * jQuery.pafRefreshPreview().
   * Refresh the preview from current paf field.
   */
  $.fn.pafRefreshPreview = function (field_name, preview) {
    //console.log(parent.jQuery('#' + field_name + '-ID').html(':)'));
    parent.jQuery('#' + field_name + '-ID').html(preview);
  };
  
  /**
   * Drupal.Panels.changed(item)
   * Extend the default panels changed method for all panels panes.
   */
  Drupal.Panels.changed = function(item) {
    if (!item.is('.changed')) {
      // Here we override the content selector items.
      if (item.hasClass('content-item')) {
        if (item.parent('.paf-content-selector').length == 0) {

          // Open the pane editor to add an item.
          $link = $("a", item);
          var region = item.parents('.panel-region').attr('id').replace("panel-region-", "");
          var base = $link.attr('href');
          var new_link = base.replace("-region-", region);
          
          // Create a drupal ajax object. By default this behavior is done
          // in panels itself by using a class "paf-use-modal" to trigger 
          // the way the modal opens, including its request URI.
          var element_settings = {};
          if ($link.attr('href')) {
            element_settings.url = new_link;
            element_settings.event = 'click';
            element_settings.progress = { type: 'none' };
          }
          Drupal.ajax[new_link] = new Drupal.ajax(new_link, $link, element_settings);

          $link.click();
          
          // Create a placeholder to drop the item where it came from.
          var placeholder = $('<div class="draggable-placeholder-object" style="display:none"></div>"');
          $('.paf-content-selector').prepend(placeholder);
          item.insertAfter(placeholder);
          placeholder.remove();
        }
      }
      // Fall-through to normal Panels behavior.
      else {
        item.addClass('changed');
        item.find('div.grabber span.text').append(' <span class="star">*</span> ');
      }
    }
  };
  
  /**
   * Onload Behavior.
   */
  Drupal.behaviors.pafOnLoad = {
    attach: function (context) {
      
      // Add draggable behavior to the content selector panes.
      var $items = $('.paf-content-selector .content-item');
      if ($items.hasClass('panel-pane') && !$items.hasClass('panel-portlet')) {
        $items
          .addClass('panel-portlet')
          .each(Drupal.Panels.bindPortlet);
      }
      
      // Trigger Panels to take our HTML into its draggables.
      //$items.find('div.grabber:not(.panel-draggable)').panelsDraggable();
      Drupal.Panels.Draggable.savePositions();
      
      // Add CTools modal ajax link behavior. This is an override
      // of the default class "ctools-use-modal".
      $('a.paf-use-modal:not(.paf-use-modal-processed)', context)
        .addClass('paf-use-modal-processed')
        // Add our click behavior first and then the ctools fallback.
        .click(Drupal.CTools.Modal.clickAjaxLink)
        .each(function () {
          // By default, panels creates a drupal ajax object here.
          // For the draggable components, the url is not known yet as 
          // we need to drag it first.
          // See Drupal.panels.changed override.
          // That's also the reason why PAF has own class "paf-use-modal".
          
          // However, when no draggables are used, we can default to 
          // normal behavior.
          if (!$(this).is('.draggable')) {
            // Create a drupal ajax object
            var element_settings = {};
            if ($(this).attr('href')) {
              element_settings.url = $(this).attr('href');
              element_settings.event = 'click';
              element_settings.progress = { type: 'none' };
            }
            var base = $(this).attr('href');
            Drupal.ajax[base] = new Drupal.ajax(base, this, element_settings);
          }
        }
      );
      
    }
  };   
 
  
})(jQuery);