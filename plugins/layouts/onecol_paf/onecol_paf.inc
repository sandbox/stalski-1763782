<?php

/**
 * implementation of hook_panels_layouts()
 */
// Plugin definition
$plugin = array(
  'title' => t('Empty single column'),
  'category' => t('Columns: 1'),
  'icon' => 'onecol.png',
  'theme' => 'panels_onecol_paf',
  'css' => 'onecol.css',
  'regions' => array('middle' => t('Middle column')),
);
