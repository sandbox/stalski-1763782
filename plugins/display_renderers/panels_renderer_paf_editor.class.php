<?php

/**
 * Renderer class for all PAF behavior.
 */
class panels_renderer_paf_editor extends panels_renderer_editor {

  /**
   * Get the links for a panel display.
   *
   */
  function get_display_links() {

    if (!$this->display->has_display_settings) {
      return;
    }

    return parent::get_display_links();

  }

  /**
   * Render the links to display when editing a region.
   */
  function get_region_links($region_id) {

    // Default panel links.
    if (!$this->display->region_links_style) {
      return parent::get_region_links($region_id);
    }

    // Content selector below the display.
    elseif ($this->display->region_links_style == PAF_REGION_LINKS_CONTENT_SELECTOR) {
      return;
    }

    // Only add content should be shown.
    $links = array();
    $links[] = array(
      'title' => t('Add content'),
      'href' => $this->get_url('select-content', $region_id),
      'attributes' => array(
        'class' => array('ctools-use-modal', 'paf-add-content'),
      ),
    );

    return theme('links', array('links' => $links, 'attributes' => array('class' => 'action-links')));

  }

  /**
   * Render the links to display when editing a pane.
   */
  function get_pane_links($pane, $content_type) {
    $links = $this->custom_pane_links($pane, $content_type);
    return theme('ctools_dropdown', array('title' => theme('image', array('path' => ctools_image_path('icon-configure.png', 'panels'))), 'links' => $links, 'image' => TRUE));
  }

  /**
   * AJAX entry point to add a new pane.
   */
  function ajax_add_pane($region = NULL, $type_name = NULL, $subtype_name = NULL, $step = NULL) {
    $content_type = ctools_get_content_type($type_name);
    $subtype = ctools_content_get_subtype($content_type, $subtype_name);

    if (!isset($step) || !isset($this->cache->new_pane)) {
      $pane = panels_new_pane($type_name, $subtype_name, TRUE);
      $this->cache->new_pane = &$pane;
    }
    else {
      $pane = &$this->cache->new_pane;
    }

    $form_state = array(
        'display' => &$this->cache->display,
        'contexts' => $this->cache->display->context,
        'pane' => &$pane,
        'cache_key' => $this->display->cache_key,
        'display cache' => &$this->cache,
        'ajax' => TRUE,
        'modal' => TRUE,
        // This will force the system to not automatically render.
        'modal return' => TRUE,
        'commands' => array(),
    );

    $form_info = array(
        'path' => $this->get_url('add-pane', $region, $type_name, $subtype_name, '%step'),
        'show cancel' => TRUE,
        'next callback' => 'panels_ajax_edit_pane_next',
        'finish callback' => 'panels_ajax_edit_pane_finish',
        'cancel callback' => 'panels_ajax_edit_pane_cancel',
    );

    $output = ctools_content_form('add', $form_info, $form_state, $content_type, $pane->subtype, $subtype, $pane->configuration, $step);

    // If $rc is FALSE, there was no actual form.
    if ($output === FALSE || !empty($form_state['complete'])) {
      // References get blown away with AJAX caching. This will fix that.
      $pane = $form_state['pane'];
      unset($this->cache->new_pane);

      // Add the pane to the display
      $this->display->add_pane($pane, $region);
      panels_edit_cache_set($this->cache);

      // Tell the client to draw the pane
      $this->command_add_pane($pane);

      // Dismiss the modal.
      $this->commands[] = ctools_modal_command_dismiss();
    }
    else if (!empty($form_state['cancel'])) {
      $this->commands[] = ctools_modal_command_dismiss();
    }
    else {
      // This overwrites any previous commands.
      $this->commands = ctools_modal_form_render($form_state, $output);
    }
  }

  /**
   * AJAX entry point to edit a pane.
   */
  function ajax_edit_pane($pid = NULL, $step = NULL) {
    if (empty($this->cache->display->content[$pid])) {
      ctools_modal_render(t('Error'), t('Invalid pane id.'));
    }

    $pane = &$this->cache->display->content[$pid];

    $content_type = ctools_get_content_type($pane->type);
    $subtype = ctools_content_get_subtype($content_type, $pane->subtype);

    // For fieldable panel panes, we load the bundle name.
    if ($pane->type == 'fieldable_panels_pane') {
      $pane->subtype_entity = fieldable_panels_panes_load_entity($pane->subtype);
      // Add it to the subtype so panels will use it in the modal title.
      $subtype['title'] = $pane->subtype_entity->bundle;
    }

    $form_state = array(
      'display' => &$this->cache->display,
      'contexts' => $this->cache->display->context,
      'pane' => &$pane,
      'display cache' => &$this->cache,
      'ajax' => TRUE,
      'modal' => TRUE,
      'modal return' => TRUE,
      'commands' => array(),
    );

    $form_info = array(
      'path' => $this->get_url('edit-pane', $pid, '%step'),
      'show cancel' => TRUE,
      'next callback' => 'panels_ajax_edit_pane_next',
      'finish callback' => 'panels_ajax_edit_pane_finish',
      'cancel callback' => 'panels_ajax_edit_pane_cancel',
    );

    $output = ctools_content_form('edit', $form_info, $form_state, $content_type, $pane->subtype,  $subtype, $pane->configuration, $step);

    // If $rc is FALSE, there was no actual form.
    if ($output === FALSE || !empty($form_state['cancel'])) {
      // Dismiss the modal.
      $this->commands[] = ctools_modal_command_dismiss();
    }
    else if (!empty($form_state['complete'])) {
      // References get blown away with AJAX caching. This will fix that.
      $this->cache->display->content[$pid] = $form_state['pane'];

      panels_edit_cache_set($this->cache);
      $this->command_update_pane($pid);
      $this->commands[] = ctools_modal_command_dismiss();
    }
    else {
      // This overwrites any previous commands.
      $this->commands = ctools_modal_form_render($form_state, $output);
    }
  }

  /**
   * Render a pane using its designated style.
   *
   * This method also manages 'title pane' functionality, where the title from
   * an individual pane can be bubbled up to take over the title for the entire
   * display.
   *
   * @param stdClass $pane
   *  A Panels pane object, as loaded from the database.
   */
  function render_pane(&$pane) {

    // For fieldable panel panes, we load the bundle name.
    if ($pane->type == 'fieldable_panels_pane') {
      list(, $fpid) = explode(":", $pane->subtype);
      $fpp = fieldable_panels_panes_load($fpid);
      $pane->type_bundle = $fpp->bundle;
    }

    // In admin, render panels_renderer_editor::render_pane().
    if ($this->admin) {
      //return parent::render_pane($pane);
      return $this->render_custom_admin_pane($pane);
    }

    $content = $this->render_pane_content($pane);
    if ($this->display->hide_title == PANELS_TITLE_PANE && !empty($this->display->title_pane) && $this->display->title_pane == $pane->pid) {

      // If the user selected to override the title with nothing, and selected
      // this as the title pane, assume the user actually wanted the original
      // title to bubble up to the top but not actually be used on the pane.
      if (empty($content->title) && !empty($content->original_title)) {
        $this->display->stored_pane_title = $content->original_title;
      }
      else {
        $this->display->stored_pane_title = !empty($content->title) ? $content->title : '';
      }
    }

    if (!empty($content->content)) {
      if (!empty($pane->style['style'])) {
        $style = panels_get_style($pane->style['style']);

        if (isset($style) && isset($style['render pane'])) {
          $output = theme($style['render pane'], array('content' => $content, 'pane' => $pane, 'display' => $this->display, 'style' => $style, 'settings' => $pane->style['settings']));

          // This could be null if no theme function existed.
          if (isset($output)) {
            return $output;
          }
        }
      }

      // fallback.
      return theme('paf_pane', array('content' => $content, 'pane' => $pane, 'display' => $this->display));

      //return theme('panels_pane', array('content' => $content, 'pane' => $pane, 'display' => $this->display));
    }
  }

  /**
   * Creates custom pane links.
   * @param $pane The panel pane
   * @param $content_type The CTools content type
   * @param $in_group Indicates if the links will are meant for dropdown.
   */
  function custom_pane_links(&$pane, $content_type) {

    $links = array();

    $subtype = ctools_content_get_subtype($content_type, $pane->subtype);

    if (ctools_content_editable($content_type, $subtype, $pane->configuration)) {
      $links[] = array(
        'title' => isset($content_type['edit text']) ? $content_type['edit text'] : t('Settings'),
        'href' => $this->get_url('edit-pane', $pane->pid),
        'attributes' => array('class' => array('ctools-use-modal', 'pane-edit')),
      );
    }

    $links[] = array(
      'title' => t('Remove'),
      'href' => '#',
      'attributes' => array(
        'class' => array('pane-delete'),
        'id' => "pane-delete-panel-pane-$pane->pid",
      ),
    );

    return $links;

  }

  /**
   * Acts as override of panels_renderer_editor::render_pane.
   */
  function render_custom_admin_pane(&$pane) {

    ctools_include('content');
    $content_type = ctools_get_content_type($pane->type);

    if (module_exists('fpp_components') && $pane->type == 'fieldable_panels_pane') {

      // Call load_multiple. Because new panes don't have numeric id.
      list(, $entity_id) = explode(':', $pane->subtype);
      $fpp_entities = fieldable_panels_panes_load_multiple(array($entity_id));
      if ($fpp_entities) {
        $fpp_entity = current($fpp_entities);
        $pane->subtype_info = ctools_content_get_subtype($content_type, $fpp_entity->bundle);
        $pane->type_bundle = $fpp_entity->bundle;
      }

    }

    // This is just used for the title bar of the pane, not the content itself.
    // If we know the content type, use the appropriate title for that type,
    // otherwise, set the title using the content itself.
    $title = ctools_content_admin_title($content_type, $pane->subtype, $pane->configuration, $this->display->context);
    if (!$title) {
      $title = t('Deleted/missing content type @type', array('@type' => $pane->type));
    }

    // If pane links aren't overridden, render administrative buttons for pane.
    if (!$this->display->pane_links_style) {
      $buttons = $this->get_pane_links($pane, $content_type);
    }

    $block = new stdClass();
    if (empty($content_type)) {
      $block->title = '<em>' . t('Missing content type') . '</em>';
      $block->content = t('This pane\'s content type is either missing or has been deleted. This pane will not render.');
    }
    else {
      $block = ctools_content_admin_info($content_type, $pane->subtype, $pane->configuration, $this->display->context);
      $block->content = theme('paf_pane', array('content' => $this->render_pane_content($pane), 'pane' => $pane, 'display' => $this->display));
    }

    // The block title.
    if (!$block->title) {
      //$block->title = t('No title');
      $block->title = empty($pane->subtype_info) ? t('No title') : $pane->subtype_info['title'];
    }

    $grabber_class = 'grab-title grabber';
    // If there are region locks, add them.
    if (!empty($pane->locks['type'])) {
      if ($pane->locks['type'] == 'regions') {
        $settings['Panels']['RegionLock'][$pane->pid] = $pane->locks['regions'];
        drupal_add_js($settings, 'setting');
      }
      else if ($pane->locks['type'] == 'immovable') {
        $grabber_class = 'grab-title not-grabber';
      }
    }

    // Add classes to control the css.
    $class = 'panel-pane';
    if (empty($pane->shown)) {
      $class .= ' hidden-pane';
    }
    if (isset($this->display->title_pane) && $this->display->title_pane == $pane->pid) {
      $class .= ' panel-pane-is-title';
    }
    if ($this->display->pane_links_style) {
      $class .= ' pane-links-overridden';
    }
    else {
      $class .= ' pane-links-normal';
    }

    // The grabber widget.
    $grabber = '<div class="paf-pane-grabber ' . $grabber_class . '">';
    if (isset($buttons)) {
      $grabber .= '<span class="buttons">' . $buttons . '</span>';
    }
    if ($this->display->pane_links_style) {
     $title = '';
    }
    $grabber .= '<span class="text">' . $title . '</span>';
    $grabber .= '</div>'; // grabber

    // Start building the output.
    $output = '<div class="' . $class . '" id="panel-pane-' . $pane->pid . '">';

    // Add our links widget.
    if ($this->display->pane_links_style) {
      $links = $this->custom_pane_links($pane, $content_type);
      $output .= $grabber;
      $output .= '<div class="paf-pane-links">';
      $output .= theme_links(array('links' => $links, 'attributes' => array(), 'heading' => ''));
      $output .= '</div>';
    }
    else {
      $output .= $grabber;
    }

    // Build the pane content.
    $output .= '<div class="paf-pane-content panel-pane-collapsible">';

    // Create the bundle icon if possible.
    if (isset($pane->subtype_info)) {
      $variables = array('path' => ctools_content_admin_icon($pane->subtype_info), 'width' => '50', 'height' => '32');
      $output .= '<div class="paf-pane-bundle">' . theme('image', $variables) . '</div>';
    }

    $output .= '<div class="pane-title">' . $block->title . '</div>';
    $output .= '<div class="pane-content">' . filter_xss_admin(render($block->content)) . '</div>';
    $output .= '</div>'; // panel-pane-collapsible

    $output .= '</div>'; // panel-pane

    return $output;
  }

}
