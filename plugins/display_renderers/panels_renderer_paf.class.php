<?php

/**
 * Renderer class for all PAF behavior.
 */
class panels_renderer_paf extends panels_renderer_standard {

  /**
   * Render a pane using its designated style.
   *
   * This method also manages 'title pane' functionality, where the title from
   * an individual pane can be bubbled up to take over the title for the entire
   * display.
   *
   * @param stdClass $pane
   *  A Panels pane object, as loaded from the database.
   */
  function render_pane(&$pane) {

    // @todo: This is heavy to do on frontend.
    ctools_include('content');
    $content_type = ctools_get_content_type($pane->type);

    // For fieldable panel panes, we load the bundle name.
    if ($pane->type == 'fieldable_panels_pane') {
      $fpp_bundle = fieldable_panels_panes_load_entity($pane->subtype);
      $pane->subtype_info = ctools_content_get_subtype($content_type, $fpp_bundle->bundle);
      $pane->type_bundle = $fpp_bundle->bundle;
    }

    // We need to take over the pane content as well.
    $content = $this->render_pane_content($pane);

    if ($this->display->hide_title == PANELS_TITLE_PANE && !empty($this->display->title_pane) && $this->display->title_pane == $pane->pid) {

      // If the user selected to override the title with nothing, and selected
      // this as the title pane, assume the user actually wanted the original
      // title to bubble up to the top but not actually be used on the pane.
      if (empty($content->title) && !empty($content->original_title)) {
        $this->display->stored_pane_title = $content->original_title;
      }
      else {
        $this->display->stored_pane_title = !empty($content->title) ? $content->title : '';
      }
    }

    if (!empty($content->content)) {

      // If a style is configured, use that one.
      if (!empty($pane->style['style'])) {
        $style = panels_get_style($pane->style['style']);

        if (isset($style) && isset($style['render pane'])) {
          $output = theme($style['render pane'], array('content' => $content, 'pane' => $pane, 'display' => $this->display, 'style' => $style, 'settings' => $pane->style['settings']));

          // This could be null if no theme function existed.
          if (isset($output)) {
            return $output;
          }
        }
      }

      // Fallback when no style is found. This will trigger the PAF theme function.
      return theme('paf_pane', array('content' => $content, 'pane' => $pane, 'display' => $this->display));

    }
  }

}