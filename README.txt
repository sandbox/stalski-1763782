

-- SUMMARY --

For a full description of the module, visit the project page:
  http://drupal.org/project/paf
  
To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/paf


-- INSTALLATION AND CONFIGURATION --


-- MAINTAINERS --

stalski - http://drupal.org/user/322618
zuuperman - http://drupal.org/user/361625
swentel - http://drupal.org/user/107403


-- THANKS TO -- 
Dieter Beheydt (for providing the nice wireframes)
Davy Van Den Bremt (for providing the concept)
Leen Van Severen (for creating the look and feel)




--- PATCHES YOU MIGHT WANT ---

 * Wysiwyg support in CTools modal
  
    If you have problems with Wysiwyg and custom content pages,
    you need the patch from http://drupal.org/node/356480, either
    - http://drupal.org/node/356480#comment-5748744
    - 121: http://drupal.org/node/356480#comment-6011572
    
 * Fieldable panels pane rendering
 
    By default, there is not rendering on the administration side
    in Fieldable panels panes. Following patch enables this feature:
    - http://drupal.org/node/1616764